package id.co.nexsoft.restapi.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.hibernate.JDBCException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.co.nexsoft.restapi.model.Media;
import id.co.nexsoft.restapi.model.Product;
import id.co.nexsoft.restapi.model.Shop;
import id.co.nexsoft.restapi.service.UUIDService;
import id.co.nexsoft.restapi.service.LongService;
import id.co.nexsoft.restapi.service.ObjectStringService;
import id.co.nexsoft.restapi.service.RelationLongService;
import id.co.nexsoft.restapi.service.RelationStringService;
import id.co.nexsoft.restapi.utils.StatusCode;
import jakarta.validation.Valid;

@RestController
@RequestMapping("/api/v1/data/product")
@Validated
public class ProductController extends StatusCode {
    @Autowired
    private UUIDService<Product> service;

    @Autowired
    private LongService<Shop> shopService;

    @Autowired
    private RelationStringService<Media> mediaRelService;

    @Autowired
    private RelationLongService<Product> relService;

    @Autowired
    private ObjectStringService<Product> productService;

    @GetMapping("/all/data")
    public ResponseEntity<?> getAllData() {
        List<Product> data = service.getAllData();
        return new ResponseEntity<>(data, HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<?> getAllActiveData() {
        List<Product> data = service.getAllActiveData();
        List<Map<String, Object>> dataList = new ArrayList<>();
        for (Product usr : data) {
            dataList.add(returnData(usr));
        }
        return new ResponseEntity<>(dataList, HttpStatus.OK);
    }

    @GetMapping("/nonactive")
    public ResponseEntity<?> getAllNonActiveData() {
        List<Product> data = service.getAllNonActiveData();
        List<Map<String, Object>> dataList = new ArrayList<>();
        for (Product usr : data) {
            dataList.add(returnData(usr));
        }
        return new ResponseEntity<>(dataList, HttpStatus.OK);
    }

    @GetMapping("/all/data/{id}")
    public ResponseEntity<?> getDataById(@PathVariable String id) {
        Optional<Product> data = service.getDataById(id);
        return (!data.isPresent()) ? 
            new ResponseEntity<>(get404(), HttpStatus.NOT_FOUND) : 
            new ResponseEntity<>(returnData(data.get()), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getActiveDataById(@PathVariable String id) {
        Optional<Product> data = service.getActiveDataById(id);
        return (!data.isPresent()) ? 
            new ResponseEntity<>(get404(), HttpStatus.NOT_FOUND) : 
            new ResponseEntity<>(returnData(data.get()), HttpStatus.OK);
    }

    @GetMapping("/shop/{id}")
    public ResponseEntity<?> getActiveDataByShopId(@PathVariable Long id) {
        List<Product> data = relService.getDataByRelId(id);
        return new ResponseEntity<>(data, HttpStatus.OK);
    }

    @GetMapping("/nonactive/{id}")
    public ResponseEntity<?> getNonActiveDataById(@PathVariable String id) {
        Optional<Product> data = service.getNonActiveDataById(id);
        return (!data.isPresent()) ? 
            new ResponseEntity<>(get404(), HttpStatus.NOT_FOUND) : 
            new ResponseEntity<>(returnData(data.get()), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteData(@PathVariable String id) {
        boolean dataDelete = service.deleteDataById(id);
        return (!dataDelete) ? 
            new ResponseEntity<>(get404(), HttpStatus.NOT_FOUND) : 
            new ResponseEntity<>(get200(), HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> putData(@Valid @RequestBody Product data, @PathVariable String id) {
        productService.putData(data, id);
        return new ResponseEntity<>(data, HttpStatus.OK);
    }   

    @PostMapping
    public ResponseEntity<?> postData(@Valid @RequestBody Product data) {
        Product dataPut = productService.postData(data);
        return (dataPut == null) ?
            new ResponseEntity<>(get422(), HttpStatus.UNPROCESSABLE_ENTITY) : 
            new ResponseEntity<>(dataPut, HttpStatus.CREATED);
    }

    @PatchMapping("/{id}")
    public ResponseEntity<?> patchData(@PathVariable String id, @RequestBody Map<String, Object> data) {
        Optional<Product> dataPatch = service.getDataById(id);
        try {
            if (!dataPatch.isPresent()) {
                return new ResponseEntity<>(get404(), HttpStatus.NOT_FOUND);
            }
            service.patchData(data, id);
            return new ResponseEntity<>(get200(), HttpStatus.OK);
        } catch (JDBCException e) {
            return new ResponseEntity<>(
                getCustom(CODE[4], extractJDBCErrorMessage(e)), 
                HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    public Map<String, Object> returnData(Product usr) {
        Map<String, Object> dataMap = new HashMap<>();
        Map<String, Object> newAddress = usr.getShopId() != null ? shopData(shopService.getDataById(usr.getShopId()).get()) : null;
        dataMap.put("id", usr.getId());
        dataMap.put("shop", newAddress);
        dataMap.put("categoryId", usr.getCategoryId());
        dataMap.put("name", usr.getName());
        dataMap.put("description", usr.getDescription());
        dataMap.put("price", usr.getPrice());
        dataMap.put("discountPrice", usr.getDiscountPrice());
        dataMap.put("stock", usr.getStock());
        dataMap.put("media", mediaData(usr.getId()));
        return dataMap;
    }

    public Map<String, Object> shopData(Shop data) {
        Map<String, Object> returnData = new HashMap<>();
        returnData.put("id", data.getId());
        returnData.put("name", data.getName());
        return returnData;
    }

    public List<Map<String, Object>> mediaData(String id) {
        List<Map<String, Object>> returnData = new ArrayList<>();
        List<Media> media = mediaRelService.getDataByRelId(id);
        for (Media m : media) {
            Map<String, Object> newData = new HashMap<>();
            newData.put("id", m.getId());
            newData.put("name", m.getName());
            newData.put("url", m.getUrl());
            newData.put("type", m.getType());
            returnData.add(newData);
        }
        return returnData;
    }
}
