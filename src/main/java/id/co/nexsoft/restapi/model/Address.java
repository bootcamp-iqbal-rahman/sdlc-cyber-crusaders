package id.co.nexsoft.restapi.model;

import java.time.LocalDateTime;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotNull;

@Entity
@Table(name = "address")
public class Address {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private String id;

    private String userId;

    @NotNull(message = "Street can not be null")
    private String street;

    @NotNull(message = "District can not be null")
    private String district;

    @NotNull(message = "City can not be null")
    private String city;

    @NotNull(message = "Province can not be null")
    private String province;

    @NotNull(message = "Country can not be null")
    private String country;
    private String code;

    private LocalDateTime createdDate;
    private LocalDateTime deletedDate;

    public Address() {}

    public Address( String userId, String street, String district, String city, 
                    String province, String country, String code, LocalDateTime createdDate, LocalDateTime deletedDate) {
        this.userId = userId;
        this.street = street;
        this.district = district;
        this.city = city;
        this.province = province;
        this.country = country;
        this.code = code;
        this.createdDate = createdDate;
        this.deletedDate = deletedDate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public LocalDateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public LocalDateTime getDeletedDate() {
        return deletedDate;
    }

    public void setDeletedDate(LocalDateTime deletedDate) {
        this.deletedDate = deletedDate;
    }
}
