package id.co.nexsoft.restapi.model;

import java.time.LocalDateTime;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotNull;

@Entity
@Table(name = "shop")
public class Shop {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull(message = "Email must not be null")
    @Email(message = "Email doesn't valid")
    private String email;

    @NotNull(message = "Password must not be null")
    private String password;

    @NotNull(message = "Phone must not be null")
    private String phone;

    @NotNull(message = "Name must not be null")
    private String name;
    private String description;

    private Long shopAddressId;

    private LocalDateTime createdDate;
    private LocalDateTime deletedDate;

    public Shop() {}

    public Shop(String email, String password, String phone, String name, String description, 
                Long shopAddressId, LocalDateTime createdDate, LocalDateTime deletedDate) {
        this.email = email;
        this.password = password;
        this.phone = phone;
        this.name = name;
        this.description = description;
        this.shopAddressId = shopAddressId;
        this.createdDate = createdDate;
        this.deletedDate = deletedDate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public LocalDateTime getDeletedDate() {
        return deletedDate;
    }

    public void setDeletedDate(LocalDateTime deletedDate) {
        this.deletedDate = deletedDate;
    }

    public Long getShopAddressId() {
        return shopAddressId;
    }

    public void setShopAddressId(Long shopAddressId) {
        this.shopAddressId = shopAddressId;
    }
}
