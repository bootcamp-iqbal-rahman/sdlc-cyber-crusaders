package id.co.nexsoft.restapi.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import id.co.nexsoft.restapi.model.Media;

public interface MediaRepository extends JpaRepository<Media, Long> {
    @Query("SELECT u FROM Media u WHERE u.productId = :id")
    List<Media> findAllByRelId(@Param("id") String id);
}
