package id.co.nexsoft.restapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import id.co.nexsoft.restapi.model.PaymentDetail;

public interface PaymentDetailRepository extends JpaRepository<PaymentDetail, Long> {
}
