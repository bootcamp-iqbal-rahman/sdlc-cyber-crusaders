package id.co.nexsoft.restapi.repository;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import id.co.nexsoft.restapi.model.Product;
import jakarta.transaction.Transactional;

public interface ProductRepository extends JpaRepository<Product, String> {
    List<Product> findAllByDeletedDateIsNull();
    List<Product> findAllByDeletedDateIsNotNull();

    @Query("SELECT u FROM Product u WHERE u.shopId = :id AND deletedDate IS NULL")
    List<Product> findAllByUserId(@Param("id") Long id);

    @Query("SELECT u FROM Product u WHERE u.id = :id AND deletedDate IS NULL")
    Optional<Product> findActiveDataById(@Param("id") String id);

    @Query("SELECT u FROM Product u WHERE u.id = :id AND deletedDate IS NOT NULL")
    Optional<Product> findNonActiveDataById(@Param("id") String id);

    @Transactional
    @Modifying
    @Query(value = "INSERT INTO product (shop_id, category_id, name, description, price, discount_price, stock, ordered_stock) VALUES (:#{#data['shopId']}, :#{#data['categoryId']}, :#{#data['name']}, :#{#data['description']}, :#{#data['price']}, :#{#data['discountPrice']}, :#{#data['stock']}, :#{#data['ordered_stock']})", nativeQuery = true)
    void addData(@Param("data") Map<String, Object> data);

    @Transactional
    @Modifying
    @Query(value = "UPDATE product SET shop_id = :#{#data['shopId']}, category_id = :#{#data['categoryId']}, name = :#{#data['name']}, description = :#{#data['description']}, price = :#{#data['price']}, discount_price = :#{#data['discountPrice']}, stock = :#{#data['stock']}, ordered_stock = :#{#data['orderedStock']} WHERE id = :id", nativeQuery = true)
    void putData(@Param("data") Map<String, Object> data, @Param("id") String id);
}
