package id.co.nexsoft.restapi.service.impl;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.nexsoft.restapi.model.Shipper;
import id.co.nexsoft.restapi.repository.ShipperRepository;
import id.co.nexsoft.restapi.service.SimpleService;
import jakarta.persistence.EntityManager;
import jakarta.persistence.Query;
import jakarta.transaction.Transactional;

@Service
public class ShipperServiceImpl implements SimpleService<Shipper> {
    @Autowired
    private ShipperRepository repo;

    @Autowired
    private EntityManager entityManager;

    @Override
    public List<Shipper> getAllData() {
        return repo.findAll();
    }

    @Override
    public Optional<Shipper> getDataById(Long id) {
        return repo.findById(id);
    }


    @Override
    public boolean deleteDataById(Long id) {
        Optional<Shipper> Shipper = repo.findById(id);
        if (!Shipper.isPresent()) {
            return false;
        }
        repo.deleteById(id);
        return true;
    }

    @Override
    public Shipper putData(Shipper data, Long id) {
        data.setId(id);
        return repo.save(data);
    }

    @Override
    public Shipper postData(Shipper data) {
        return repo.save(data);
    }

    @Transactional
    @Override
    public void patchData(Map<String, Object> data, Long id) {
        StringBuilder queryBuilder = new StringBuilder("UPDATE Shipper SET ");

        for (Map.Entry<String, Object> entry : data.entrySet()) {
            queryBuilder.append(entry.getKey()).append(" = :").append(entry.getKey()).append(", ");
        }

        queryBuilder.delete(queryBuilder.length() - 2, queryBuilder.length());
        queryBuilder.append(" WHERE id = :id");

        Query query = entityManager.createNativeQuery(queryBuilder.toString());

        for (Map.Entry<String, Object> entry : data.entrySet()) {
            query.setParameter(entry.getKey(), entry.getValue());
        }

        query.setParameter("id", id);
        query.executeUpdate();
    }
}