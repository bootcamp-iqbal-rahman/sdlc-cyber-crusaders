package id.co.nexsoft.restapi.service.impl;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.nexsoft.restapi.model.Wishlist;
import id.co.nexsoft.restapi.repository.WishlistRepository;
import id.co.nexsoft.restapi.service.RelationStringService;
import id.co.nexsoft.restapi.service.SimpleService;
import jakarta.persistence.EntityManager;
import jakarta.persistence.Query;
import jakarta.transaction.Transactional;

@Service
public class WishlistServiceImpl implements SimpleService<Wishlist>, RelationStringService<Wishlist> {
    @Autowired
    private WishlistRepository repo;

    @Autowired
    private EntityManager entityManager;

    @Override
    public List<Wishlist> getAllData() {
        return repo.findAll();
    }

    @Override
    public Optional<Wishlist> getDataById(Long id) {
        return repo.findById(id);
    }


    @Override
    public boolean deleteDataById(Long id) {
        Optional<Wishlist> Wishlist = repo.findById(id);
        if (!Wishlist.isPresent()) {
            return false;
        }
        repo.deleteById(id);
        return true;
    }

    @Override
    public Wishlist putData(Wishlist data, Long id) {
        data.setId(id);
        return repo.save(data);
    }

    @Override
    public Wishlist postData(Wishlist data) {
        return repo.save(data);
    }

    @Transactional
    @Override
    public void patchData(Map<String, Object> data, Long id) {
        StringBuilder queryBuilder = new StringBuilder("UPDATE Wishlist SET ");

        for (Map.Entry<String, Object> entry : data.entrySet()) {
            queryBuilder.append(entry.getKey()).append(" = :").append(entry.getKey()).append(", ");
        }

        queryBuilder.delete(queryBuilder.length() - 2, queryBuilder.length());
        queryBuilder.append(" WHERE id = :id");

        Query query = entityManager.createNativeQuery(queryBuilder.toString());

        for (Map.Entry<String, Object> entry : data.entrySet()) {
            query.setParameter(entry.getKey(), entry.getValue());
        }

        query.setParameter("id", id);
        query.executeUpdate();
    }

    @Override
    public List<Wishlist> getDataByRelId(String userId) {
        return repo.findAllByRelId(userId);
    }
}