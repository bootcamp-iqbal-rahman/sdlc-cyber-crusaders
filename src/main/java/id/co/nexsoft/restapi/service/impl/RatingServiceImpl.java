package id.co.nexsoft.restapi.service.impl;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.nexsoft.restapi.model.Rating;
import id.co.nexsoft.restapi.repository.RatingRepository;
import id.co.nexsoft.restapi.service.RelationStringService;
import id.co.nexsoft.restapi.service.SimpleService;
import jakarta.persistence.EntityManager;
import jakarta.persistence.Query;
import jakarta.transaction.Transactional;

@Service
public class RatingServiceImpl implements SimpleService<Rating>, RelationStringService<Rating> {
    @Autowired
    private RatingRepository repo;

    @Autowired
    private EntityManager entityManager;

    @Override
    public List<Rating> getAllData() {
        return repo.findAll();
    }

    @Override
    public Optional<Rating> getDataById(Long id) {
        return repo.findById(id);
    }


    @Override
    public boolean deleteDataById(Long id) {
        Optional<Rating> Rating = repo.findById(id);
        if (!Rating.isPresent()) {
            return false;
        }
        repo.deleteById(id);
        return true;
    }

    @Override
    public Rating putData(Rating data, Long id) {
        data.setId(id);
        return repo.save(data);
    }

    @Override
    public Rating postData(Rating data) {
        return repo.save(data);
    }

    @Transactional
    @Override
    public void patchData(Map<String, Object> data, Long id) {
        StringBuilder queryBuilder = new StringBuilder("UPDATE rating SET ");

        for (Map.Entry<String, Object> entry : data.entrySet()) {
            queryBuilder.append(entry.getKey()).append(" = :").append(entry.getKey()).append(", ");
        }

        queryBuilder.delete(queryBuilder.length() - 2, queryBuilder.length());
        queryBuilder.append(" WHERE id = :id");

        Query query = entityManager.createNativeQuery(queryBuilder.toString());

        for (Map.Entry<String, Object> entry : data.entrySet()) {
            query.setParameter(entry.getKey(), entry.getValue());
        }

        query.setParameter("id", id);
        query.executeUpdate();
    }

    @Override
    public List<Rating> getDataByRelId(String userId) {
        return repo.findAllByRelId(userId);
    }


}