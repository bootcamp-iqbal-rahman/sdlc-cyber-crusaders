package id.co.nexsoft.restapi.service.impl;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.nexsoft.restapi.model.Shop;
import id.co.nexsoft.restapi.repository.ShopRepository;
import id.co.nexsoft.restapi.service.LongService;
import id.co.nexsoft.restapi.service.ObjectLongService;
import id.co.nexsoft.restapi.service.CredentialService;
import id.co.nexsoft.restapi.utils.GetPassword;
import jakarta.persistence.EntityManager;
import jakarta.persistence.Query;
import jakarta.transaction.Transactional;

@Service
public class ShopServiceImpl extends GetPassword implements LongService<Shop>, CredentialService<Shop>, ObjectLongService<Shop> {
    @Autowired
    private ShopRepository repo;

    @Autowired
    private EntityManager entityManager;

    @Override
    public List<Shop> getAllData() {
        return repo.findAll();
    }

    @Override
    public List<Shop> getAllActiveData() {
        return repo.findAllByDeletedDateIsNull();
    }

    @Override
    public List<Shop> getAllNonActiveData() {
        return repo.findAllByDeletedDateIsNotNull();
    }

    @Override
    public Optional<Shop> getDataById(Long id) {
        return repo.findById(id);
    }

    @Override
    public Optional<Shop> getActiveDataById(Long id) {
        return repo.findActiveDataById(id);
    }

    @Override
    public Optional<Shop> getNonActiveDataById(Long id) {
        return repo.findNonActiveDataById(id);
    }

    @Override
    public boolean deleteDataById(Long id) {
        Optional<Shop> Shop = repo.findActiveDataById(id);
        if (!Shop.isPresent()) {
            return false;
        }
        Shop.get().setDeletedDate(LocalDateTime.now());
        return true;
    }

    @Override
    public Shop putData(Shop data, Long id) {
        Optional<Shop> shops = repo.findById(id);
        if (!shops.isPresent()) {
            return null;
        }
        data.setId(id);
        return repo.save(data);
    }

    @Override
    public Shop postData(Shop data) {
        data.setPassword(hashPassword(data.getPassword()));
        data.setCreatedDate(LocalDateTime.now());
        return repo.save(data);
    }

    @Override
    public List<Shop> getAllDataByEmail(String email) {
        return repo.findAllByEmail(email);
    }

    @Override
    public List<Shop> getAllDataByPhone(String phone) {
        return repo.findAllByPhone(phone);
    }

    @Transactional
    @Override
    public void patchData(Map<String, Object> data, Long id) {
        StringBuilder queryBuilder = new StringBuilder("UPDATE shop SET ");

        for (Map.Entry<String, Object> entry : data.entrySet()) {
            queryBuilder.append(entry.getKey()).append(" = :").append(entry.getKey()).append(", ");
        }

        queryBuilder.delete(queryBuilder.length() - 2, queryBuilder.length());
        queryBuilder.append(" WHERE id = :id");

        Query query = entityManager.createNativeQuery(queryBuilder.toString());

        for (Map.Entry<String, Object> entry : data.entrySet()) {
            query.setParameter(entry.getKey(), entry.getValue());
        }

        query.setParameter("id", id);
        query.executeUpdate();
    }

}
